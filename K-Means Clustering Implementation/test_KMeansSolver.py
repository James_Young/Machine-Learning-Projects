import unittest
import numpy as np
from KMeansSolver import KMeansSolver

class TestKMeansSolver(unittest.TestCase):

    def setUp(self):
        self.Solver = KMeansSolver()
        self.nExamples = 10
        self.nFeatures = 2
        self.data = np.ones((self.nExamples, self.nFeatures))
    
    def test_load_data(self):
        self.Solver.load_data(self.data)
        self.assertIsNotNone(self.Solver.get_data())
        self.assertTrue(np.array_equal(self.Solver.get_data(), self.data))
        self.assertEqual(self.Solver.get_nFeatures(), self.nFeatures)
        self.assertEqual(self.Solver.get_nExamples(), self.nExamples)

    def test_initialise_clusters(self):
        self.Solver.load_data(self.data) 
        self.assertRaises(TypeError, self.Solver.initialiseCentroids())

        self.Solver.set_nClusters(1)
        self.assertEqual(self.Solver.get_nClusters(), 1)
        self.Solver.initialiseCentroids()

if __name__ == "__main__":
    unittest.main()