import numpy as np 
import matplotlib.pyplot as plt

class KMeansSolver:

    def __init__(self):
        self.__nClusters = None
        self.__nFeatures = 0
        self.__nExamples = 0
        self.__data = None
        self.__clusterAssignment = None
        self.__previousCentroids = None 

        self.__centroids = None
        self.__CONVERGED = False

    def load_data(self, data):
        self.__nFeatures = data.shape[1]
        self.__nExamples = data.shape[0]
        self.__data = data
        self.__clusterAssignment = np.zeros(self.__nExamples)
        
    def get_data(self):
        return self.__data

    def get_nFeatures(self):
        return self.__nFeatures
    
    def get_nExamples(self):
        return self.__nExamples

    def get_nClusters(self):
        return self.__nClusters

    def set_nClusters(self, nClusters):
        self.__nClusters = nClusters

    def initialiseCentroids(self):
        try:
            if self.__nClusters is None:
                raise TypeError
            
        except TypeError:
            print("nClusters is not set")
            return
        
        self.__centroids = np.zeros((self.__nClusters, self.__nFeatures))
        for i in range(self.__centroids.shape[0]):
            self.__centroids[i][:] = self.chooseDataPoint()
    

    def chooseDataPoint(self):
        index = np.random.randint(self.__nExamples)
        return self.__data[index][:]
    
    def assignDataToClusters(self):
        for i in range(self.__nExamples):
            example = self.__data[i][:]
            self.__clusterAssignment[i] = self.assignPointToCluster(example)
    
    def assignPointToCluster(self, point):
        dist = np.zeros((self.__nClusters))
        for i in range(self.__nClusters):
            centroid = self.__centroids[i]
            dist[i] = self.distanceBetween(point, centroid)
        return np.argmin(dist)

    def distanceBetween(self, point1, centroid):
        return np.sum((point1 - centroid)**2)
    
    def moveCentroids(self):
        self.__previousCentroids = self.__centroids
        for i in range(self.__nClusters):
            isPartOfCluster = self.__clusterAssignment == i
            if not any(isPartOfCluster):
                continue
            selectedData = self.__data[isPartOfCluster]

            self.__centroids[i] = self.findNewCentroid(selectedData)
    
    def findNewCentroid(self, data):
        return np.mean(data, axis=0)

    def hasConverged(self):
        return np.array_equal(self.__previousCentroids, self.__centroids)

    def solveNTimes(self, nClusters, nTimes=25):
        J = []
        results = []
        for i in range(nTimes):
            self.solveOnce(nClusters)
            J.append(self.getCost())
            results.append(self.__centroids)
        return [J, results]

    def solve(self, nClusters):
        J, results = self.solveNTimes(nClusters)
        self.__centroids = results[np.argmin(J)]
        return np.min(J)
        

    def solveOnce(self, nClusters):
        self.set_nClusters(nClusters)
        self.initialiseCentroids()
        self.assignDataToClusters()

        while not self.hasConverged():
            self.moveCentroids()
            self.assignDataToClusters()
    
    def getCost(self):
        cost = 0
        for i in range(self.__nClusters):
            centroid = self.__centroids[i]
            isPartOfCluster = self.__clusterAssignment == i
            selectedData = self.__data[isPartOfCluster] 
            
            cost += np.sum((selectedData - centroid)**2)
        return cost / self.__nExamples
    
    def predict(self, data):
        prediction = np.zeros(data.shape[0])
        for i in range(data.shape[0]):
            point = data[i][:]
            prediction[i] = self.assignPointToCluster(point)
        return prediction
 
def generateCluster(x, y, sigma, count):
    x = np.random.normal(x, sigma, count)
    y = np.random.normal(y, sigma, count)
    return [x, y]

def generateClusters(n):
    x = []
    y = []
    for i in range(n):
        x_center = np.random.uniform(-1,1,1)
        y_center = np.random.uniform(-1,1,1)
        x_coords, y_coords = generateCluster(x_center, y_center, 0.1, 10) 
        x += list(x_coords)
        y += list(y_coords)
    return np.array([x, y]).T


def normalise(x):
    return (x - np.mean(x, axis=0)) / np.std(x, axis=0)

def main():

    data = generateClusters(5)

    data = normalise(data)
    
    KM = KMeansSolver()
    KM.load_data(data)
    J = []
    for i in range(1,8):
        print(i)
        J.append(KM.solve(i))
    
    KM.solve(5) 
    prediction = KM.predict(data)
    c = ['r', 'g', 'b', 'c', 'y']
    color = [c[int(item)] for item in prediction]
    
    plt.subplot(1,2,1)
    plt.plot(np.arange(1,8), J)
    plt.subplot(1,2,2)
    plt.scatter(data[:,0], data[:,1], c=color)
    plt.show()



if __name__ == "__main__":
    main()